# codeing = utf-8
# -*- coding:utf-8 -*-
# Author：直言
import json
import allure
import pytest
import tools_pintuan
import yaml


# GET /api/group/order/list?current=1&orderState=3&size=10&type=1  #已拼中

@allure.feature('验证订单拼中结果')  #模块
class Test_order:
    url = ""

    @classmethod  #使用pytest 命令时，执行用例前的初始化方法，自动调用
    def setup_class(init):
        init.hostname = "https://test428.kuxiaoxiao.com"
        f = open('test_data.yaml', 'r', encoding='utf-8')
        cfg = f.read()
        d = yaml.load(cfg, Loader=yaml.FullLoader)
        pt_mobile_list = d[0]["pt_mobile"]
        if len(pt_mobile_list) < 2:
            print("至少配置两个拼团手机号:",pt_mobile_list)
            exit(1)
        Test_order.data_list = []
        for mobile in pt_mobile_list:
            data_dict = {}
            token = getTokenByPhone(mobile)
            if token != None and len(token) > 1:
                data_dict["mobile"] = mobile
                data_dict["uuid"] = Test_order.uuid
                data_dict["token"] = token
                print(data_dict)
                Test_order.data_list.append(data_dict)
        if len(Test_order.data_list) < 2:
            print("至少配置两个有效拼团手机号:", Test_order.data_list)




        Test_order.token = getTokenByPhone('13401157249')
        print('token==',Test_order.token)
        if init.token == None:
            print('token获取异常')
            exit(1)

    @pytest.mark.run(order=1)
    @allure.story('商品列表')      #子模块
    def test_getProduct(self):               #用例

        url = '%s/api/group/product/getProduct' % self.hostname
        data = r'{"pageNum":"1","pageSize":"10"}'
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)

        # print('res === ',res.data.decode())
        if res.status != 200:
            print('httpCode==%d',res.code())
        print('code=========',json.loads(res.data.decode()).get("code"))
        data_json = json.loads(res.data.decode())
        product_list = data_json["data"]["list"]["records"]
        Test_order.skuId = None
        Test_order.price = 0
        Test_order.productId = None
        for product in product_list:
            if product["publishStatus"] == 1 and product["stock"] > 10:
                Test_order.productId = product["id"]
                Test_order.price = product["price"]
                Test_order.groupId = product["groupId"]
                print('商品价格：',Test_order.price)
                break
        assert r'200' == str(data_json.get("code")) and len(data_json.get("data").get("list").get("records")) > 0

    @pytest.mark.run(order=2)
    @allure.story('商品详情')      #子模块
    def test_product_selectProduct(self):               #用例
        if self.productId == None:
            print('未找到商品:',self.productId)
            exit(-1)
        url = '%s/api/group/product/selectProduct' % self.hostname
        data = r'{"id":"%s"}' % self.productId
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)

        # print('res === ',res.data.decode())
        if res.status != 200:
            print('httpCode==%d',res.code())
        print('code=========',json.loads(res.data.decode()).get("code"))
        data_json = json.loads(res.data.decode())
        print('res ====',data_json)
        # print(data_json.get("data").get("list").get("records"))
        skuList = data_json.get("data").get("product").get("skuVOList")[0]
        print('skuList===',skuList)
        Test_order.skuId = skuList["skuId"]
        Test_order.skuPromotionPrice = skuList["skuPromotionPrice"]
        Test_order.skuAttrValueNames = skuList["skuAttrValueNames"]
        assert r'200' == str(data_json.get("code")) and len(data_json.get("data").get("product").get("skuVOList")) > 0


    @pytest.mark.run(order=3)
    @allure.story('商品详情')      #子模块
    def test_checkParent(self):               #用例
        # if self.productId == None:
        #     print('未找到商品:',self.productId)
        #     exit(-1)
        url = '%s/api/personal/info/checkParent' % self.hostname
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)

        # print('res === ',res.data.decode())
        if res.status != 200:
            print('httpCode==%d',res.code())
        print('code=========',json.loads(res.data.decode()).get("code"))
        data_json = json.loads(res.data.decode())
        assert r'200' == str(data_json.get("code"))  and data_json.get("data").get("status") == True

    @pytest.mark.run(order=4)
    @allure.story('成团')  # 子模块
    def test_pt(self):  # 用例
        res_list = []
        orderList = []
        status = True
        count = 0
        index = 0
        while status:
            count += 1
            if count > len(Test_order.data_list) * 5:

                print('拼团结果：%s'%res_list)
                assert len(res_list) > 0
                break
            data = Test_order.data_list[index % len(Test_order.data_list)]
            tools_pintuan.mysqlexec(
                "update kxx_xxk_dev.pt_member_asset set integral_out_balance = 1000 where member_uuid = %s" % Test_order.data_list[index % len(Test_order.data_list)]["uuid"])
            index += 1
            Test_order.token = data["token"]
            Test_order.uuid = data["uuid"]
            Test_order.mobile = data["mobile"]
            print('执行开始。。。。。。。')
            print('data === ', data)
            # Test_order.test_product_checkOrder(self)
            res = Test_order.order_addorder(self)
            if json.loads(res.data.decode())["code"] == 200:
                orderId = json.loads(res.data.decode())["data"]["orderId"]
                data["orderId"] = orderId
                orderList.append(data)
            print('执行结束。。。。。。。')

            orderList_size = len(orderList)
            if orderList_size > 1:
                order1 = None
                while orderList_size > 0:
                    res_list = []
                    data = orderList[orderList_size-1]
                    sql = 'select group_status,status,rebate_status,parent_id from kxx_xxk_dev.pt_product_assemble where order_id = %s' % orderId
                    sql_res = tools_pintuan.mysqlexec(sql)
                    if sql_res[0][0] == 1:
                        status = False
                        orderList_size = -1
                        res_list += sql_res

                        if sql_res[0][3] == 0:
                            sql = 'select group_status,status,rebate_status,parent_id from kxx_xxk_dev.pt_product_assemble where parent_id = "%s"' % (data["order_id"])
                            sql_res = tools_pintuan.mysqlexec(sql)
                            res_list += sql_res
                        else:
                            sql = 'select group_status,status,rebate_status,parent_id from kxx_xxk_dev.pt_product_assemble where parent_id = "%s" or order_id = %s' % (
                            data["parent_id"],data["parent_id"])
                            res_list = sql_res

                        # sql = 'select join_num,join_success_num,join_integral from  kxx_xxk_dev.pt_activity_rule where status =1 and is_del = 0 and id = %d' % Test_order.groupId
                        # sql_res = tools_pintuan.mysqlexec(sql)
                        join_num = len(res_list)
                        if join_num != 10:
                            print('拼团人数【%d】不正确,与预期【%d】不符' %(join_num,int(sql_res[0][0])))
                            assert  int(sql_res[0][0]) == join_num

                        json_success_num = 0
                        for res in sql_res:
                            if res[1] == 1:
                                json_success_num = json_success_num + 1
                        if json_success_num != 1:
                            print('拼中人数【%d】不正确,与预期【%d】不符' % (json_success_num, int(sql_res[0][1])))
                            assert int(sql_res[0][1]) == json_success_num

                        # todo asset data judge
                    orderList_size = orderList_size -1





    # @allure.story('积分参团')      #子模块
    # @pytest.mark.run(order=5)
    def order_addorder(self):               #用例
        token = getTokenByPhone(self.mobile)
        url = '%s/api/group/order/addOrder' % self.hostname
        data = r'{"skuId":%s,"buyNum":1,"activityId":1}' % self.skuId
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % token}
        print('用户id：%s  , 手机号：%s' %(self.uuid,self.mobile))
        res = tools_pintuan.httpRequest('POST',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        return res
        # assert 200 == res_dict.get("code")




    @allure.story('我的团队')      #子模块
    def test_asset_getVipLevel(self):               #用例
        url = '%s/api/group/asset/getVipLevel' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @classmethod  #使用pytest 命令时，执行用例前的初始化方法，自动调用
    def teardown_class(end):

        print("运行结束end")


if __name__ == '__main__':
    pytest.main(['-s', '-q', '--alluredir', './report/xml'])
    print('111111111111111111111111111111122222222222222222222222222222222222')


def getTokenByPhone(phone):
    token = ''
    uuid = tools_pintuan.mysqlexec("select member_uuid from kxx_dev.ums_member where phone = '%s'" % phone)
    print("uuid ===",uuid)
    if uuid != None and uuid != [] and len(uuid[0][0]) > 0:
        key = 'i_kxx-auth:jwt-token:%s:PTAPP'% uuid[0][0]
        Test_order.uuid = uuid[0][0]
        print('key===',key)
        token = tools_pintuan.redisexec('get',key,'')
        print('token===',token)
        return token
    return token


#Test_1.test_1(Test_1)

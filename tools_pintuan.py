# codeing = utf-8
# -*- coding:utf-8 -*-
# Author：直言

import pymongo
import json
import urllib3
import time
import pymysql
import datetime
import yaml
import redis



f = open('test_data.yaml', 'r', encoding='utf-8')
cfg = f.read()
d = yaml.load(cfg, Loader=yaml.FullLoader) # 用load转字典

Loader=yaml.FullLoader
# print('d==============',d[0]["pt_mobile"][1])


def mysqlexec(sqltext):
    print('mysql_host========',d[0]["mysql_host"])
    conn = pymysql.connect(host=d[0]["mysql_host"],
                           port=d[0]["mysql_port"],
                           user=d[0]["mysql_user"],
                           passwd=d[0]["mysql_passwd"],
                           db=d[0]["mysql_db"],
                           charset='utf8')

    # conn = pymysql.connect(host='172.17.0.136', port=3306, user='dev_rr', passwd='527UsBPwlwY8yDrPn78H',
    #                        db='kxx_xxk_pro', charset='utf8')

    row_data = ''

    try:
        cursor = conn.cursor()
        # print('mysql开始执行：',sqltext)
        effect_row = cursor.execute(sqltext)
        conn.commit()
        if effect_row == 0:
            print('sql执行无数据:',sqltext)
            return []

        #row_data = cursor.fetchone()
        datalist = []
        while effect_row > 0:
            effect_row = effect_row - 1
            row_1 = cursor.fetchone()
            datalist.append(row_1)

        # print('mysql执行结果datalist===：',datalist)
    except Exception as e:
        print(e)
        return []
    finally:
        conn.close()

    return datalist

def httpRequest(method,url,data,header):
    urllib3.disable_warnings()
    http = urllib3.PoolManager()
    print('http请求 开始------------------------------------------------------------------------------------------------------------')
    print(method,'\t',url)
    print(data)
    if str.upper(method) == 'POST':
        recv = http.request("POST", url, body=data.encode(), headers=header, timeout=30)
    if str.upper(method) == 'GET':
        recv = http.request("GET", url, body=data.encode(), headers=header, timeout=30)
    print(recv.status)
    try:
        print('http请求结果: ', recv.data.decode())
    except:
        print('http请求结果: ', recv.data)
    print('http请求 结束------------------------------------------------------------------------------------------------------------')
    return recv


def redisexec(method,key,value):
    res = ''
    try:
        r = redis.Redis(host=d[0]["redis_host"], port=d[0]["redis_port"], decode_responses=True)
        if str.upper(method) == 'GET':

            return r.get(key)
        if str.upper(method) == 'SET':
            return r.set(key,value)
    except:
        res = None
    return res


def redisexec1(method,key,value):
    res = ''
    try:
        r = redis.Redis(host=d[0]["redis_host"], port=d[0]["redis_port"],db=1, decode_responses=True)
        if str.upper(method) == 'GET':

            return r.get(key)
        if str.upper(method) == 'SET':
            return r.set(key,value)
    except:
        res = None
    return res


def getTokenByPhone(phone):
    token = ''
    uuid = mysqlexec("select member_uuid from kxx_dev.ums_member where phone = '%s'" % phone)
    print('uuid====',uuid[0][0])
    if uuid != None and len(uuid[0][0]) > 0:
        key = 'i_kxx-auth:jwt-token:%s:PTAPP'% uuid[0][0]
        print('key===',key)
        token = redisexec('get',key,'')
        print('token===',token)
        return token
    return token

token = getTokenByPhone('13401157249')
print('token=',token)
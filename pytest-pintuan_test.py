# codeing = utf-8
# -*- coding:utf-8 -*-
# Author：直言
import json
import allure
import pytest
import tools_pintuan



@allure.feature('校验接口是否响应（code==200）')  #模块
class Test_pintuan:
    url = ""

    @classmethod  #使用pytest 命令时，执行用例前的初始化方法，自动调用
    def setup_class(init):
        init.hostname = "https://test428.kuxiaoxiao.com"
        init.token = getTokenByPhone('13401157249')

        if init.token != None:
            tools_pintuan.redisexec1('set','13401157249',init.token)


        tools_pintuan.mysqlexec("update kxx_xxk_dev.pt_member_asset set integral_out_balance = 1000 where member_uuid = %s" % Test_pintuan.uuid)
        print('token==',init.token)
        if init.token == None:
            print('token获取异常')
            init.token = tools_pintuan.redisexec1('get', '13401157249', init.token)
        if init.token == None:
            print('token获取异常')
            exit(1)
    @pytest.mark.run(order=1)
    @allure.story('商品列表')      #子模块
    def test_getProduct(self):               #用例

        url = '%s/api/group/product/getProduct' % self.hostname
        data = r'{"pageNum":"1","pageSize":"10"}'
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)

        # print('res === ',res.data.decode())
        if res.status != 200:
            print('httpCode==%d',res.status)
        print('code=========',json.loads(res.data.decode()).get("code"))
        data_json = json.loads(res.data.decode())
        product_list = data_json.get("data").get("list").get("records")
        Test_pintuan.skuId = None
        Test_pintuan.price = 0
        Test_pintuan.productId = None
        for product in product_list:
            if product["publishStatus"] == 1 and product["stock"] > 10:
                Test_pintuan.productId = product["id"]
                Test_pintuan.price = product["price"]
                Test_pintuan.groupId = product["groupId"]
                print('商品价格：',Test_pintuan.price)
                break
        assert r'200' == str(data_json.get("code")) and len(data_json.get("data").get("list").get("records")) > 0

    @pytest.mark.run(order=2)
    @allure.story('商品详情')      #子模块
    def test_product_selectProduct(self):               #用例
        if self.productId == None:
            print('未找到商品:',self.productId)
            exit(-1)
        url = '%s/api/group/product/selectProduct' % self.hostname
        data = r'{"id":"%s"}' % self.productId
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)

        # print('res === ',res.data.decode())
        if res.status != 200:
            print('httpCode==%d',res.status)
        print('code=========',json.loads(res.data.decode()).get("code"))
        data_json = json.loads(res.data.decode())
        print('res ====',data_json)
        # print(data_json.get("data").get("list").get("records"))
        skuList = data_json.get("data").get("product").get("skuVOList")[0]
        print('skuList===',skuList)
        Test_pintuan.skuId = skuList["skuId"]
        Test_pintuan.skuPromotionPrice = skuList["skuPromotionPrice"]
        Test_pintuan.skuAttrValueNames = skuList["skuAttrValueNames"]
        assert r'200' == str(data_json.get("code")) and len(data_json.get("data").get("product").get("skuVOList")) > 0


    @pytest.mark.run(order=3)
    @allure.story('商品详情')      #子模块
    def test_checkParent(self):               #用例
        # if self.productId == None:
        #     print('未找到商品:',self.productId)
        #     exit(-1)
        url = '%s/api/personal/info/checkParent' % self.hostname
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)

        # print('res === ',res.data.decode())
        if res.status != 200:
            print('httpCode==%d',res.status)
        print('code=========',json.loads(res.data.decode()).get("code"))
        data_json = json.loads(res.data.decode())
        assert r'200' == str(data_json.get("code"))  and data_json.get("data").get("status") == True


    @pytest.mark.run(order=4)
    @allure.story('积分参团')      #子模块
    def test_product_checkOrder(self):               #用例
        sql = "delete from kxx_xxk_dev.pt_product_assemble where member_uuid = %s" %(Test_pintuan.uuid)
        tools_pintuan.mysqlexec(sql)
        url = '%s/api/group/product/checkOrder?groupId=%s' % (Test_pintuan.hostname,Test_pintuan.groupId)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % Test_pintuan.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("用户执行失败--uuid::",Test_pintuan.uuid,res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")




    @pytest.mark.run(order=4)
    @allure.story('积分参团')      #子模块
    def test_order_verifyOrder(self):               #用例
        url = '%s/api/group/order/verifyOrder' % (self.hostname)
        data = '{"skuId":"%s","buyNum":"1"}' % self.skuId
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @pytest.mark.run(order=4)
    @allure.story('积分参团')      #子模块
    def test_cert_validateAuth(self):               #用例
        url = '%s/api/group/cert/validateAuth' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @pytest.mark.run(order=4)
    @allure.story('积分参团')      #子模块
    def test_order_advertisingPopUp(self):               #用例
        url = '%s/api/group/order/advertisingPopUp' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")



    @pytest.mark.run(order=4)
    @allure.story('积分参团')      #子模块
    def test_image_generate(self):               #用例
        url = '%s/api/group/image/generate' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        # res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res.status != 200:
            print("http返回:http：",res.status)
            exit(res.status)
        assert 200 == res.status


    @allure.story('积分参团')      #子模块
    @pytest.mark.run(order=5)
    def test_order_addorder(self):               #用例
        key = 'i_group-center:imageKey:IMAGE_CODE_KEY_%s' % self.uuid
        imageCode = tools_pintuan.redisexec('get',key,'')
        if imageCode == None or imageCode == 'null':
            print('获取图片验证码异常：',key,imageCode)
            exit(1)
        url = '%s/api/group/order/addOrder?imageCode=%s' % (self.hostname,str(imageCode))
        data = r'{"skuId":%s,"buyNum":1,"activityId":1}' % self.skuId
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        assert 200 == res_dict.get("code")




    @allure.story('我的团队')      #子模块
    def test_asset_getVipLevel(self):               #用例
        url = '%s/api/group/asset/getVipLevel' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('我的团队')      #子模块
    def test_invitation_brushInvitationList(self):               #用例
        url = '%s/api/group/user/invitation/brushInvitationList' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('我的团队')      #子模块
    def test_invitation_getInvitationAndExtensionList(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/user/invitation/getInvitationAndExtensionList?current=1&orderType=1&size=20&type=1' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('登录后首页接口')      #子模块
    def test_personal_info(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/personal/info' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('登录后首页接口')      #子模块
    def test_android_versions_getVersionsIos(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/android_versions/getVersionsIos' % (self.hostname)
        data = '{"versionsNumber":"0"}'
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('POST',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")



    @allure.story('我的订单')      #子模块
    def test_order_list(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/list?current=1&orderState=&size=10&type=2' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        else:
            Test_pintuan.orderId = res_dict["data"]["records"][0]["orderId"]
            print("订单号：",Test_pintuan.orderId)
        assert 200 == res_dict.get("code")


    @allure.story('我的订单')      #子模块
    def test_order_orderDetails(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/orderDetails?orderId=%s&type=2' % (self.hostname,self.orderId)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @allure.story('我的订单')      #子模块
    def test_order_list_State1(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/list?current=1&orderState=1&size=10&type=2' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('我的订单')      #子模块
    def test_order_list_State2(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/list?current=1&orderState=2&size=10&type=2' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @allure.story('我的订单')      #子模块
    def test_order_list_State3(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/list?current=1&orderState=3&size=10&type=2' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('拼团记录')      #子模块
    def test_statistics_ptRecord(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/statistics/ptRecord' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @allure.story('拼团记录')      #子模块
    def test_asset_getGroupSum(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/asset/getGroupSum' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('拼团记录')      #子模块
    def test_order_list_State2_type1(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/list?current=1&orderState=2&size=10&type=1' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('拼团记录')      #子模块
    def test_order_list_State4_type1(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/order/list?current=1&orderState=4&size=10&type=1' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('我的收益')      #子模块
    def test_asset_getDetailPage(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/asset/getDetailPage?crossType=0&current=1&size=10&type=3' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @allure.story('我的收益')      #子模块
    def test_statistics_expensesRecord_Type1(self):               #用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/statistics/expensesRecord?expensesType=1&page=1&size=20' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json","Authorization":"%s" % self.token}
        res = tools_pintuan.httpRequest('GET',url,data,header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("执行失败:",res_dict)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('我的收益')  # 子模块
    def test_statistics_expensesRecord_Type2(self):  # 用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/statistics/expensesRecord?expensesType=2&page=1&size=20' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json", "Authorization": "%s" % self.token}
        res = tools_pintuan.httpRequest('GET', url, data, header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("%s：执行失败", self.__class__)
            exit(res_dict)
        assert 200 == res_dict.get("code")

    @allure.story('我的收益')  # 子模块
    def test_statistics_expensesRecord_Type3(self):  # 用例
        # token = getTokenByPhone('13401157249')
        url = '%s/api/group/statistics/expensesRecord?expensesType=3&page=1&size=20' % (self.hostname)
        data = ''
        header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json", "Authorization": "%s" % self.token}
        res = tools_pintuan.httpRequest('GET', url, data, header)
        res_dict = json.loads(res.data.decode())
        # print('res === ',res.data.decode())
        if res_dict.get("code") != 200:
            print("%s：执行失败", self.__class__)
            exit(res_dict)
        assert 200 == res_dict.get("code")


    @classmethod  #使用pytest 命令时，执行用例前的初始化方法，自动调用
    def teardown_class(end):

        print("运行结束end")


if __name__ == '__main__':
    pytest.main(['-s', '-q', '--alluredir', './report/xml'])
    print('111111111111111111111111111111122222222222222222222222222222222222')


def getTokenByPhone(phone):
    token = ''
    uuid = tools_pintuan.mysqlexec("select member_uuid from kxx_dev.ums_member where phone = '%s'" % phone)
    print('uuid====',uuid[0][0])
    if uuid != None and len(uuid[0][0]) > 0:
        key = 'i_kxx-auth:jwt-token:%s:PTAPP'% uuid[0][0]
        Test_pintuan.uuid = uuid[0][0]
        print('key===',key)
        token = tools_pintuan.redisexec('get',key,'')
        print('token===',token)
        return token
    return token


#Test_1.test_1(Test_1)
